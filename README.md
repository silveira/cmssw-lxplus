# El7 image with condor support for lxplus

This repository holds the `Dockerfile` for an `el7` image that allows to login to `lxplus9` while emulating logging into `lxplus7.cern.ch`. It is based on the `cmssw/el7:x86_64`, which is the image you get when you issue `cmssw-el7`, but is also capable of submitting condor jobs.

## Usage
To use it, log onto lxplus and, the first time, create a `start_el7.sh` script with the following content:
```
#!/bin/bash
export APPTAINER_BINDPATH=/afs,/cvmfs,/cvmfs/grid.cern.ch/etc/grid-security:/etc/grid-security,/cvmfs/grid.cern.ch/etc/grid-security/vomses:/etc/vomses,/eos,/etc/pki/ca-trust,/etc/tnsnames.ora,/run/user,/tmp,/var/run/user,/etc/sysconfig,/etc:/orig/etc
schedd=`myschedd show -j | jq .currentschedd | tr -d '"'`

apptainer -s exec /cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/cms-cat/cmssw-lxplus/cmssw-el7-lxplus:latest/ sh -c "source /app/setupCondor.sh && export _condor_SCHEDD_HOST=$schedd && export _condor_SCHEDD_NAME=$schedd && export _condor_CREDD_HOST=$schedd && /bin/bash  "
```
Make it executable and then run: 
```
./start_el7.sh
```

This will drop you in a `el7` shell, equivalent to logging to `lxplus7`. You will have access to LXBatch HTcondor as well. Your jobs, by default, will be handled by the same schedd that you are normally assigned to, but you can switch schedd by doing:

```
export _condor_SCHEDD_HOST=bigbirdXY.cern.ch
export _condor_SCHEDD_NAME=bigbirdXY.cern.ch
export _condor_CREDD_HOST=bigbirdXY.cern.ch
```
with appropriate values for `XY`.

You can use this image in your condor jobs as well, by adding to your submission file:
```
MY.SingularityImage = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/cms-cat/cmssw-lxplus/cmssw-el7-lxplus:latest/"
```
